<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/*
 * This middleware class handle the admin endpoint check
 * The user table 'role' column is checked
*/

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if ($user['role'] === 'admin')
        {
            return $next($request);
        }
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }
}
