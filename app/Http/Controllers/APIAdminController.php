<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Loans;

class APIAdminController extends Controller {

    /**
     * Create a new controller instance.
     * @codeCoverageIgnore
     * @return void
     */
    public function __construct() {
        $this->middleware(['auth:api', 'admin']);
    }

    public function AuthRouteAPI(Request $request) {
        return $request->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $user = $request->user();

        return response()->json($request->user(), 200);
    }

    /**
     * Display loan requests
     *
     * @return \Illuminate\Http\Response
     */
    public function listLoanRequests() {
        $loans = DB::table('loans')->where('status', 'pending')->get();

        return response()->json($loans, 200);
    }

    /**
     * Approve a requests
     *
     * @return \Illuminate\Http\Response
     */
    public function approveRequests(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Enter a valid loan id'], 400);
        }

        $data = $request->input();
        $loan = DB::table('loans')->where('id', $data['id'])->first();

        $emi = round(($loan->amount_required*1.1)/$loan->loan_term, 2);
        $amount_pending = $emi*$loan->loan_term;
        $remaining_term = $loan->loan_term;

        DB::table('loans')
            ->where('id', $data['id'])
            ->update(['status' => 'approved', 'emi' => $emi, 'amount_pending' => $amount_pending, 'remaining_term' => $remaining_term]);

        return response()->json(['message' => 'success'], 200);
    }

}
