<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Loans;

class APIUserController extends Controller {

    /**
     * Create a new controller instance.
     * @codeCoverageIgnore
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function AuthRouteAPI(Request $request) {
        return $request->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $user = $request->user();

        return response()->json($request->user(), 200);
    }

    /**
     * Apply for loan
     *
     * @return \Illuminate\Http\Response
     */
    public function applyLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount_required' => 'required|integer',
            'loan_term' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'required fields are missing'], 400);
        }

        $user = $request->user();
        if (Loans::where('user', '=', $user['id'])->exists()) {
           return response()->json(['message' => 'Loan already exist'], 400);
        }

        $data = $request->input();
        try{

            $loan = new Loans;
            $loan->user = $user['id'];
            $loan->amount_required = $data['amount_required'];
            $loan->loan_term = $data['loan_term'];
            $loan->save();

            return response()->json($loan, 201);
        }
        catch(Exception $e){
            return response()->json(['message' => 'Some error occured!'], 403);
        }
    }

    /**
     * Display loan details
     *
     * @return \Illuminate\Http\Response
     */
    public function loanDetails(Request $request) {
        $user = $request->user();
        $loan = DB::table('loans')->where('user', $user['id'])->first();

        return response()->json($loan, 200);
    }

    /**
     * Loan repayment
     *
     * @return \Illuminate\Http\Response
     */
    public function repayLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'required fields are missing'], 400);
        }

        $user = $request->user();
        $loan = Loans::where('user', '=', $user['id'])->first();
        if ($loan) {
            if (($loan->remaining_term <= 0) || (!$loan->amount_pending <= 0)) {
               return response()->json(['message' => 'Loan doesnot exist'], 400);
            }

            $data = $request->input();
            $remaining_term = ($loan->remaining_term - 1);
            $amount_pending = ($loan->amount_pending - $data['amount']);

            try{
                $loan->remaining_term = $remaining_term;
                $loan->amount_pending = $amount_pending;
                $loan->save();

                return response()->json($loan, 200);
            }
            catch(Exception $e){
                return response()->json(['message' => 'Some error occured!'], 403);
            }
        } else {
            return response()->json(['message' => 'Loan doesnot exist'], 400);
        }        
    }

}
