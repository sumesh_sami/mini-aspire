<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIUserController as APIUser;
use App\Http\Controllers\APIAdminController as APIAdmin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* user APIs */
Route::get('/user/me', [APIUser::class, 'index']);
Route::get('/user/me/loan', [APIUser::class, 'loanDetails']);
Route::post('/user/me/loan', [APIUser::class, 'applyLoan']);
Route::post('/user/me/loan/payment', [APIUser::class, 'repayLoan']);

/* Admin APIs */
Route::get('/admin/me', [APIAdmin::class, 'index']);
Route::get('/admin/loan', [APIAdmin::class, 'listLoanRequests']);
Route::post('/admin/loan', [APIAdmin::class, 'approveRequests']);

/* Handle the 404 with json response */
Route::fallback(function(){
    return response()->json(['message' => 'Page Not Found'], 404);
});