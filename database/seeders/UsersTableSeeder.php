<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
        \DB::table('users')->insert([
            'name' => 'Sumesh',
            'email' => 'sumeshpch@gmail.com',
            'password' => bcrypt('sumesh78'),
            'role' => 'user'
        ]);
        \DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@miniaspire.com',
            'password' => bcrypt('password'),
            'role' => 'admin'
        ]);
    }
    }
}
