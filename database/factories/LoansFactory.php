<?php

namespace Database\Factories;

use App\Models\Loans;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoansFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Loans::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user' => 1,
            'amount_required' => 10000,
            'loan_term' => 100,
        ];
    }
}
