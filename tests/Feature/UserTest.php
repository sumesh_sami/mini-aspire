<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Passport\Passport;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        Passport::actingAs(User::factory()->create(), []);

        $response = $this->json('GET', '/api/user/me');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                    'id',
                    'name',
                    'email',
                    'role'
                ]);
    }
}
