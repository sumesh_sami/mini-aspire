<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Loans;
use Laravel\Passport\Passport;

class AdminTest extends TestCase
{
    /**
     * Loan lists
     *
     * @return void
     */
    public function test_loanDetails()
    {
        Passport::actingAs(User::factory()->create(['role' => 'admin']), []);

        $response = $this->json('GET', '/api/admin/loan');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([]);
    }

    /**
     * Approve a loan
     *
     * @return void
     */
    public function test_loanApprove()
    {
        Passport::actingAs(User::factory()->create(['role' => 'admin']), []);

        $response = $this->json(
            'POST',
            '/api/admin/loan',
            [
                'id' => 1
            ]);

        $response
            ->assertStatus(200);
    }

    /**
     * Approve a loan
     *
     * @return void
     */
    public function test_InvalidloanApprove()
    {
        Passport::actingAs(User::factory()->create(['role' => 'admin']), []);

        $response = $this->json(
            'POST',
            '/api/admin/loan',
            [
                'id' => ''
            ]);

        $response
            ->assertStatus(400);
    }
}
