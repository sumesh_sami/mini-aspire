<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Loans;
use Laravel\Passport\Passport;

class LoanTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_PostLoan()
    {
        Passport::actingAs(User::factory()->create(), []);

        $response = $this->json(
            'POST',
            '/api/user/me/loan',
            [
                'amount_required' => 10000,
                'loan_term' => 100
            ]);

        $response
            ->assertStatus(201);
    }

    public function test_PostLoanWrong()
    {
        Passport::actingAs(User::factory()->create(), []);

        //wrong loan properties
        $response1 = $this->json(
            'POST',
            '/api/user/me/loan',
            [
                'loan_term' => 100
            ]);
        $response2 = $this->json(
            'POST',
            '/api/user/me/loan',
            [
                'amount_required' => 10000
            ]);

        $response1
            ->assertStatus(400);
        $response2
            ->assertStatus(400);
    }

    public function test_loanDetails()
    {
        Passport::actingAs(User::factory()->create(), []);

        $response = $this->json('GET', '/api/user/me/loan');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([]);
    }

    public function test_LoanPayment()
    {
        Passport::actingAs(User::factory()->create(), []);

        //wrong loan properties
        $response = $this->json(
            'POST',
            '/api/user/me/loan/payment',
            [
                'amount' => 110
            ]);

        $response
            ->assertStatus(400);
    }
}
