# Steps used to create this project

*** create a new laravel project using latest version of composer
composer create-project --prefer-dist laravel/laravel mini-aspire

*** change directory to the new project folder
cd mini-aspire

*** install laravel-passport package. This will handle the user authentication
composer require laravel/passport

*** now create required database and add the database credentials in the .env file

*** create the migration file required to store the client and access token
php artisan migrate

*** create the encryption keys needed to generate secured access tokens
php artisan passport:install

*** Before your application can issue tokens via the password grant, you will need to create a password grant client
php artisan passport:client --password

*** create the user table seeder class to add dummy users to the application
php artisan make:seeder UsersTableSeeder

*** create the dummu users to the table
php artisan db:seed

*** push the new project to git
git remote add origin <git-repo-url>


# Steps to use the project in a different machine

1. Clone your project using the git url [ssh/https]

2. Go to the folder application using cd command on your cmd or terminal

3. Run composer install on your cmd or terminal

4. Copy .env.example file to .env on the root folder. You can type copy .env.example .env if using command prompt Windows or cp .env.example .env if using terminal, Ubuntu

5. Open your .env file and change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.

6. Run php artisan key:generate

7. Run php artisan migrate

8. Run php artisan serve

9. Open "http://localhost:8000" in postman

# Deployment script is added which can be used while deploying the application to the server